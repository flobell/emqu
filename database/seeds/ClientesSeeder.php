<?php

use Illuminate\Database\Seeder;

class ClientesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clientes')->insert([
            'rif' => 'PEDRO FLORES',
            'razon' => 'J-24848217-0',
            'ciudad' => 'guayana'
        ]);
    }
}
