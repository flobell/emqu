<?php

use Illuminate\Database\Seeder;

class MetodoPagosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('metodo_pagos')->insert([
            ['nombre' => 'TARJETA DE DEBITO'],
            ['nombre' => 'TARJETA DE CREDITO'],
            ['nombre' => 'CHEQUE'],
            ['nombre' => 'EFECTIVO']
        ]);
    }
}
