<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(CajerosSeeder::class);
        $this->call(CategoriasSeeder::class);
        $this->call(ClientesSeeder::class);
        $this->call(FormatosSeeder::class);
        $this->call(FranquiciasSeeder::class);
        $this->call(MetodoPagosSeeder::class);
        $this->call(ProductosSeeder::class);
        
    }
}
