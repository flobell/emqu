<?php

use Illuminate\Database\Seeder;

class FranquiciasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('franquicias')->insert([
            ['nombre' => 'INVERSIONES ASTRO C.A'],
            ['nombre' => 'EMPRESA PRUEBA C.A'],
            ['nombre' => 'SUPERMERCADO C.A']
        ]);
    }
}
