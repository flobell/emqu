<?php

use Illuminate\Database\Seeder;

class CajerosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cajeros')->insert([
            ['nombre' => 'PEDRO','numero_caja' => 1],
            ['nombre' => 'JUAN','numero_caja' => 2],
            ['nombre' => 'LUIS','numero_caja' => 3],
            ['nombre' => 'HECTOR','numero_caja' => 4]
        ]);
    }
}
