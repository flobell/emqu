<?php

use Illuminate\Database\Seeder;

class ProductosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('productos')->insert([
            ['nombre' => "HARINA DE MAIZ",'descripcion' => 'NULL','monto_unitario' => "99.99",'id_formato' => 4,'id_categoria' => 1,'stock' => 100,'fecha_vencimiento' => "2020-11-30"],
            ['nombre' => "LECHE LIQUITA",'descripcion' => 'NULL','monto_unitario' => "99.99",'id_formato' => 5,'id_categoria' => 1,'stock' => 100,'fecha_vencimiento' => "2020-11-30"],
            ['nombre' => "HARINA DE TRIGO",'descripcion' => 'NULL','monto_unitario' => "99.99",'id_formato' => 4,'id_categoria' => 1,'stock' => 100,'fecha_vencimiento' => "2020-11-30"],
            ['nombre' => "ACEITE VEGETAL",'descripcion' => 'NULL','monto_unitario' => "99.99",'id_formato' => 5,'id_categoria' => 1,'stock' => 100,'fecha_vencimiento' => "2020-11-30"],
            ['nombre' => "MARGARINA MAVESA",'descripcion' => 'NULL','monto_unitario' => "99.99",'id_formato' => 4,'id_categoria' => 1,'stock' => 100,'fecha_vencimiento' => "2020-11-30"],
        ]);
    }
}
