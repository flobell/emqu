<?php

use Illuminate\Database\Seeder;

class CategoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('categoria_productos')->insert([
            ['nombre' => 'VIVERES', 'descripcion' => 'NULL'],
            ['nombre' => 'CONFITERIA', 'descripcion' => 'NULL'],
            ['nombre' => 'HIGIENE', 'descripcion' => 'NULL'],
            ['nombre' => 'LIMPIEZA', 'descripcion' => 'NULL'],
            ['nombre' => 'BEBES', 'descripcion' => 'NULL'],
            ['nombre' => 'MASCOTAS', 'descripcion' => 'NULL'],
            ['nombre' => 'BEBIDAS', 'descripcion' => 'NULL'],
            ['nombre' => 'VARIOS', 'descripcion' => 'NULL'],
        ]);
    }
}
