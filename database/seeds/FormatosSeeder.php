<?php

use Illuminate\Database\Seeder;

class FormatosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('formato_productos')->insert([
            ['nombre' => 'UNIDAD'],
            ['nombre' => 'BOLSA'],
            ['nombre' => 'CAJA'],
            ['nombre' => 'KG'],
            ['nombre' => 'LT']
        ]);
    }
}
