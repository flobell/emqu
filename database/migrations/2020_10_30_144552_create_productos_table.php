<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',255)->nullable(false);
            $table->text('descripcion')->nullable(false);
            $table->decimal('monto_unitario',10,2)->nullable(false);
            // $table->string('formato')->nullable(false);
            // $table->string('categoria')->nullable(false);
            $table->unsignedBigInteger('id_formato')->nullable(false);
            $table->unsignedBigInteger('id_categoria')->nullable(false);
            $table->integer('stock')->nullable(false);
            $table->date('fecha_vencimiento')->nullable(false);
            $table->timestamps();

            $table->foreign('id_formato')->references('id')->on('formato_productos')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('id_categoria')->references('id')->on('categoria_productos')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
