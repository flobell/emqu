<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('numero_control')->nullable(false);
            $table->date('fecha_emision')->nullable(false);
            $table->string('rif_cliente',45)->nullable(false);
            $table->integer('numero_caja')->nullable(false);
            $table->unsignedBigInteger('id_franquicia')->nullable(false);
            $table->unsignedBigInteger('id_metodo_pago')->nullable(false);
            $table->decimal('subtotal',10,2)->nullable(false);
            $table->decimal('iva',10,2)->nullable(false);
            $table->timestamps();

            $table->unique('numero_control');
            $table->foreign('rif_cliente')->references('rif')->on('clientes')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('numero_caja')->references('numero_caja')->on('cajeros')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('id_franquicia')->references('id')->on('franquicias')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('id_metodo_pago')->references('id')->on('metodo_pagos')->onDelete('restrict')->onUpdate('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas');
    }
}
