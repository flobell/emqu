<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturaDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factura_detalles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('numero_control')->nullable(false);
            $table->unsignedBigInteger('id_producto')->nullable(false);
            // $table->integer('orden')->nullable(false);
            $table->integer('cantidad')->nullable(false);
            // $table->decimal('subtotal',10,2)->nullable(false);
            $table->timestamps();

            $table->foreign('numero_control')->references('numero_control')->on('facturas')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_producto')->references('id')->on('productos')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factura_detalles');
    }
}
