<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/factura/cargar','FacturaController@cargarFactura');
Route::post('/import','FacturaController@importExcel');
Route::get('/import','FacturaController@importExcel');

Route::get('/factura/buscar','FacturaController@buscar');
Route::get('/factura/detalle','FacturaController@detalle');