<?php

namespace App\Http\Controllers;

use App\factura;
use App\Imports\FacturaImport;
use Illuminate\Http\Request;
use Validator;
use App\franquicia;
use App\cajero;
use App\cliente;
use App\MetodoPago;
use Excel;
use App\Imports\FacturaDetalleImport;
use DB;

class FacturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $factura = new Factura;
        $factura->fecha_emision = $request->fecha_emision;
        $factura->rif_cliente = $request->rif_cliente;
        $factura->numero_caja = $request->numero_caja;
        $factura->id_franquicia = $request->id_franquicia;
        $factura->id_metodo_pago = $request->id_metodo_pago;
        $factura->subtotal = $request->subtotal;
        $factura->iva = $request->iva;
        $factura->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function show(factura $factura)
    {
        //
        return Factura::where('id', $factura->id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function edit(factura $factura)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, factura $factura)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function destroy(factura $factura)
    {
        //
    }

    public function cargarFactura(){
        $franquicias = Franquicia::pluck('nombre', 'id');
        $cajeros = Cajero::pluck('nombre', 'id');
        $metodos = MetodoPago::pluck('nombre', 'id');
        // dd($franquicias);
        return view('factura.cargar')
        ->with('franquicias', $franquicias)
        ->with('cajeros', $cajeros)
        ->with('metodos', $metodos);
    }

    public function importExcel(Request $request){

        // dd($request->all());
        $cliente = Cliente::firstOrNew(array('rif' => $request->rif_cliente));
        $cliente->rif =  $request->rif_cliente;
        $cliente->razon =  $request->nombre_cliente;
        $cliente->ciudad =  $request->ciudad_cliente;
        $cliente->save();

        $factura = new Factura;
        $factura->numero_control = $request->numero_control;
        $factura->fecha_emision = $request->fecha_emision;
        $factura->rif_cliente = $request->rif_cliente;
        $factura->numero_caja = $request->numero_caja;
        $factura->id_franquicia = $request->id_franquicia;
        $factura->id_metodo_pago = $request->id_metodo_pago;
        $factura->subtotal = $request->subtotal;
        $factura->iva = $request->iva;
        $factura->save();

        $validator = Validator::make($request->all(),[
            'file' => 'required|max:5000|mimes:xlsx,xls,csv'
        ]);

        if($validator->passes()){

           
            $file = $request->file('file');
            Excel::import(new FacturaDetalleImport($request->numero_control), $file);

            // $dataTime = date('Ymd_His');
            // $fileName = $dataTime . '-' . $file->getClientOriginalName();
            // // $fileName = $dataTime.'-factura';
            // $savePath = public_path('/upload/');
            // $file->move($savePath,$fileName);

            return redirect()->back()
            ->with(['success'=>'Archivo cargado exitosamente!']);
        } else {
            return redirect()->back()
            ->with(['errors'=>$validator->errors()->all()]);
        }
    }

    public function buscar(){
        return view('factura.buscar');
    }

    public function detalle(Request $request){
        $factura = $request->nro;

        $select1 = DB::select(
        "SELECT t1.id,
        t1.numero_control,
        t1.fecha_emision,
        t1.rif_cliente AS cliente_rif,
        t2.razon AS cliente_nombre,
        t3.nombre AS franquicia,
        t4.nombre AS metodo,
        t1.subtotal,
        t1.iva
        FROM emqutest.facturas AS t1
        INNER JOIN emqutest.clientes AS t2 ON (t2.rif = t1.rif_cliente)
        INNER JOIN emqutest.franquicias AS t3 ON (t3.id = t1.id_franquicia)
        INNER JOIN emqutest.metodo_pagos AS t4 ON (t4.id = t1.id_metodo_pago)
        WHERE numero_control = {$factura}");

        if($select1 == NULL){
            return redirect('factura/buscar')
            ->with(['errors'=> ['No existe la factura Nº'.$factura]]);

        }


        $select2 = DB::select(
        "SELECT 
        t1.id,
        t2.nombre,
        t1.cantidad,
        t2.monto_unitario,
        t3.nombre AS categoria,
        t4.nombre AS formato
        FROM emqutest.factura_detalles AS t1
        INNER JOIN emqutest.productos AS t2 ON(t2.id = t1.id_producto)
        INNER JOIN emqutest.categoria_productos AS t3 ON(t3.id = t2.id_categoria)
        INNER JOIN emqutest.formato_productos AS t4 ON(t4.id = t2.id_formato)
        WHERE numero_control = '{$factura}'");
        $detalles = collect($select2);

        // dd($select1);
        return view('factura.detalle')
        ->with('factura', $factura)
        ->with('principal', $select1[0])
        ->with('detalles', $detalles);
        
    }

}
