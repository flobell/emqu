<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    protected $guarded = [];
    protected $table = 'facturas';
    //
    protected $fillable = [
        'numero_control',
        'fecha_emision',
        'rif_cliente',
        'numero_caja',
        'id_franquicia',
        'id_metodo_pago',
        'subtotal',
        'iva'
    ];

}
