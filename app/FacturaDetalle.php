<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacturaDetalle extends Model
{
    //
    protected $fillable = [
        'numero_control',
        'id_producto',
        // 'orden',
        'cantidad'
    ];
}
