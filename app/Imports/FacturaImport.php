<?php

namespace App\Imports;

use App\Factura;
use Maatwebsite\Excel\Concerns\ToModel;

class FacturaImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Factura([
            //
        ]);
    }
}
