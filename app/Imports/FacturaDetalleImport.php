<?php

namespace App\Imports;

use App\FacturaDetalle;
use Maatwebsite\Excel\Concerns\ToModel;

class FacturaDetalleImport implements ToModel
{
    private $numero_control;

    public function __construct(string $numero_control) 
    {
        $this->numero_control = $numero_control;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new FacturaDetalle([
            'numero_control' => $this->numero_control,
            'id_producto' => $row[0],
            'cantidad' => $row[1]
        ]);
    }
}
