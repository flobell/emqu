@extends('layouts.app')

@section('content')

    <h1>Cargar Factura</h1>

    <form action="{{ url('/import')}}" method="POST" enctype="multipart/form-data" id="facturaForm">
        
        {{ csrf_field() }}

        @if(session('errors'))  
            @foreach ($errors as $error)
            <li>{{ $error }}</li>
            @endforeach
        @endif 
        @if(session('success'))  
            <b>{{ session('success') }}</b>
        @endif 

        <div class="form-group">
            <label for="">Seleccionar franquicia</label>
            
            <select class="form-control" name="id_franquicia" id="id_franquicia" form="facturaForm" required>
                @foreach($franquicias as $value => $franquicia)
                    <option value="{{$value}}">{{$franquicia}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="">Nº Control</label>
            <input type="number" class="form-control" name="numero_control" required>
        </div>

        <div class="form-group">
            <label for="">Fecha de Emisión</label>
            <input type="date" class="form-control" name="fecha_emision" required>
        </div>

        <div class="form-group">
            <label for="">RIF del cliente</label>
            <input type="text" class="form-control" name="rif_cliente" required>
        </div>

        <div class="form-group">
            <label for="">Nombre del cliente</label>
            <input type="text" class="form-control" name="nombre_cliente" required>
        </div>

        <div class="form-group">
            <label for="">Ciudad del cliente</label>
            <input type="text" class="form-control" name="ciudad_cliente" required>
        </div>

        <div class="form-group">
            <label for="">Nº Caja</label>
            <!-- <input type="text" class="form-control" name="numero_caja" required> -->
            <select class="form-control" name="numero_caja" id="numero_caja" form="facturaForm" required>
                @foreach($cajeros as $value => $cajero)
                    <option value="{{$value}}">{{$value}}-{{$cajero}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="">Metodo de Pago</label>
            <select class="form-control" name="id_metodo_pago" id="id_metodo_pago" form="facturaForm" required>
                @foreach($metodos as $value => $metodo)
                    <option value="{{$value}}">{{$metodo}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="">Subtotal</label>
            <input type="number" class="form-control" name="subtotal" placeholder="######,##" step=".01" required>
        </div>

        <div class="form-group">
            <label for="">IVA %</label>
            <input type="number" class="form-control" name="iva" placeholder="###" min="0" max="100" required>
        </div>
        
        <div class="form-group">
            <label for="">Adjuntar productos</label><br>
            <input type="file" name="file" id="file" required><br>
            <a href="{{ url('/sample/formato_productos_factura.xlsx') }}"> Descargar formato </a>
        </div>

        <div class="form-group" align="center">
            <button type="submit" class="btn btn-dark">Cargar</button>  
        </div>
    </form>

@stop

<!-- <!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Import Excel</title>
    </head>
    <body>

    <form action="{{ url('/import')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}

        @if(session('errors'))  
            @foreach ($errors as $error)
            <li>{{ $error }}</li>
            @endforeach
        @endif 
        @if(session('success'))  
            {{ session('success') }}
        @endif 
        <br><br>   

        Selecciones el archivo excel
        <br><br>

        <input type="file" name="file" id="file">
        <br><br>

        <button type="submit">Cargar Archivo</button>

        <br><br><br>
        <a href="{{ url('/sample/RegistroFactura.xlsx') }}"> Descargar formato </a>
    </form>

    </body>
<html> -->