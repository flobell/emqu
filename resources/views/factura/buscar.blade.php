@extends('layouts.app')

@section('content')

    <h1>Buscar Factura</h1>

    <form action="{{ url('/factura/detalle')}}" method="GET" enctype="multipart/form-data" id="facturaForm">
        
        {{ csrf_field() }}

        @if(session('errors'))  
            @foreach ($errors as $error)
            <li>{{ $error }}</li>
            @endforeach
        @endif 
        @if(session('success'))  
            <b>{{ session('success') }}</b>
        @endif 

        <div class="form-group" style="width: 25%;">
            <label for="">Nº Control</label>
            <input type="number" class="form-control" name="nro" required>
        </div>


        <div class="form-group" align="left">
            <button type="submit" class="btn btn-dark">Buscar</button>  
        </div>
    </form>

@stop
