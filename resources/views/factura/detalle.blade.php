@extends('layouts.app')

@section('content')

    <div align="center">
        <a href="{{ url()->previous() }}" class="btn btn-dark">Volver</a>   
    </div>

    <h1>FACTURA Nº {{ $factura }}</h1>

    <div >
        <ul class="list-group">
            <li class="list-group-item"><b>RIF Cliente: </b> {{ $principal->cliente_rif }}</li>
            <li class="list-group-item"><b>Nombre Cliente: </b> {{ $principal->cliente_nombre }}</li>
            <li class="list-group-item"><b>Franquicia:</b> {{ $principal->franquicia }}</li>
            <li class="list-group-item"><b>Metodo de pago:</b> {{ $principal->metodo }}</li>
            <li class="list-group-item"><b>subtotal:</b> {{ $principal->subtotal }}</li>
            <li class="list-group-item"><b>iva:</b> {{ $principal->iva }}</li>
        </ul>
    </div>

    <div class="table-responsive">
        <table class="table table-striped table-hover table-condensed">
            <thead>
            <tr>
                <th><strong>Producto</strong></th>
                <th><strong>Cantidad</strong></th>
                <th><strong>Monto Unitario</strong></th>
                <th><strong>Categoria</strong></th>
                <th><strong>Formato</strong></th>
            </tr>
            </thead>
            <tbody>
                @foreach($detalles as $key => $data)
                <tr>    
                    <th>{{$data->nombre}}</th>
                    <th>{{$data->cantidad}}</th>
                    <th>{{$data->monto_unitario}}</th>
                    <th>{{$data->categoria}}</th>
                    <th>{{$data->formato}}</th>                 
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    
    
    
    
    
    
<!-- 
    <form action="{{ url('/factura/detalle')}}" method="GET" enctype="multipart/form-data" id="facturaForm">
        
        {{ csrf_field() }}

        @if(session('errors'))  
            @foreach ($errors as $error)
            <li>{{ $error }}</li>
            @endforeach
        @endif 
        @if(session('success'))  
            <b>{{ session('success') }}</b>
        @endif 

        <div class="form-group" style="width: 25%;">
            <label for="">Nº Control</label>
            <input type="number" class="form-control" name="nro" required>
        </div>


        <div class="form-group" align="left">
            <button type="submit" class="btn btn-dark">Buscar</button>  
        </div>
    </form> -->

@stop
